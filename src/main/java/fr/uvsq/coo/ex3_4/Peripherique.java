package fr.uvsq.coo.ex3_4;

/**
 * @author amine.mezrani (uvsq21506775)
 *
 */

public interface Peripherique {
	void print();

}
