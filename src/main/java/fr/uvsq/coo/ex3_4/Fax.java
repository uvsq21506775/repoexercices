package fr.uvsq.coo.ex3_4;
/**
 * @author amine.mezrani (uvsq21506775)
 *ISP :une classe ne doit pas dependre  d'interfaces dont elle n'a pas l'utilit�
 */

import java.io.File;
import java.util.List;

public class Fax implements Peripherique {
	public void print() {
		// imprim
	}

	public void fax(List<File> f) {
		// fax
	}
}
