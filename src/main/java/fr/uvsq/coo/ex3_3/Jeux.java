package fr.uvsq.coo.ex3_3;
/**
 * @author amine.mezrani (uvsq21506775)
 *
 
* avant modification ne respectait pas LSP 
*    car robot statique qui peut avancer ne peut pas etre remplac� par 
*    un robot statique
*/

import java.util.ArrayList;

class Jeux {
	ArrayList<Robot> r;
	ArrayList<RobotStatique> rs;

	void AvanceTous() {
		int j;
		for (j = 0; j < r.size(); j++) {
			r.get(j).Avance();
		}
	}

}
