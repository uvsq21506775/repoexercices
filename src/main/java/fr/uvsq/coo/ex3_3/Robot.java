package fr.uvsq.coo.ex3_3;
/**
 * @author amine.mezrani (uvsq21506775)
 *
 */

public class Robot {
	public enum Direction {
		n, o, s, e;
	}

	private Position p;
	private Direction d;

	public void tourne() {
		if (d == Direction.n)
			d = Direction.o;
		if (d == Direction.o)
			d = Direction.s;
		if (d == Direction.s)
			d = Direction.e;
		if (d == Direction.e)
			d = Direction.n;
	}

	public void Avance() {
		if (d == Direction.n)
			p.y++;
		if (d == Direction.o)
			p.x--;
		if (d == Direction.s)
			p.y--;
		if (d == Direction.e)
			p.x++;

	}
}
