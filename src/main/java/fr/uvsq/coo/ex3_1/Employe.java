package fr.uvsq.coo.ex3_1;

/**
 * @author amine.mezrani (uvsq21506775)
 *
 */

/**
 * 1.cette classe avant modification ne respectait pas SRP car elle avait deux
 * responsabilit� cacul salaire et affichage coordonnees 
 * 2.si la m�thode de
 * calcul du salaire change la classe employe va etre modifier 
 * 3.si l�affichage
 * est remplac� par le stockage dans un fichier CSV aussi ne respecte pas
 * toujours SRP car aura toujours deux responsabilit�.
 *4.
 */

public class Employe {
	private final String nom = "";
	private final String adresse = "";

	public String getNom() {
		return nom;
	}

	public String getAdresse() {
		return adresse;
	}

}