package fr.uvsq.coo.ex3_6;
/**
 * @author amine.mezrani (uvsq21506775)
 */
import java.util.ArrayList;

public class Annuaire {
	private static Annuaire annu;
	// attribut de type interface
	private Groupe personnes;

	private Annuaire() {
		// this.personnes=new ArrayList();
	}

	public static Annuaire getAnnuaire() {
		if (annu == null) {
			annu = new Annuaire();
		}
		return annu;
	}

	public static void main(String[] args) {
		getAnnuaire();
	}

}
