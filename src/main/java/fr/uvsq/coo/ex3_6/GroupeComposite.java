package fr.uvsq.coo.ex3_6;

/**
 * @author amine.mezrani (uvsq21506775)
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GroupeComposite implements Groupe {

	private List<Personnel> p = new ArrayList<Personnel>();

	public void print() {
		Iterator<Personnel> iterator = p.iterator();
		while (iterator.hasNext()) {
			Personnel personnel = iterator.next();
			personnel.Affichage();
		}
	}

	public void add(Personnel personnel) {
		p.add(personnel);
	}

	public void remove(Personnel personnel) {
		p.remove(personnel);
	}

}