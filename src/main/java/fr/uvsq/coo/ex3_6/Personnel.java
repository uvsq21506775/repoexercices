package fr.uvsq.coo.ex3_6;

/**
* @author amine.mezrani (uvsq21506775)
*/

import java.util.List;

public final class Personnel implements Groupe  {
	private final String nom;
	private final String prenom;
	private final List<NumeroTel> num_tel;
	private final java.time.LocalDate date_nai;
	private final List<Type_Fonction> fonc;

	public static class builder {
		private final String nom;
		private final String prenom;
		private List<NumeroTel> num_tel;
		private java.time.LocalDate date_nai;
		private List<Type_Fonction> fonc;

		public builder(String n, String p) {
			this.nom = n;
			this.prenom = p;
		}

		public builder Date_nai(java.time.LocalDate d) {
			date_nai = d;
			return this;

		}

		public builder Num_tel(List<NumeroTel> num_tele) {
			num_tel = num_tele;
			return this;

		}

		public builder fonc(List<Type_Fonction> f) {
			fonc = f;
			return this;
		}
	}

	private Personnel(builder builder) {
		// Required parameters

		nom = builder.nom;
		prenom = builder.prenom;
		// Optional parameters

		num_tel = builder.num_tel;
		date_nai = builder.date_nai;
		fonc = builder.fonc;

	}

	public void Affichage() {
		System.out.println(this.nom + "" + this.prenom);
	}

	public void print() {
		// TODO Auto-generated method stub
		
	}
}
