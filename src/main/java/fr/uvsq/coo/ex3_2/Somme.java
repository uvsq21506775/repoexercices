package fr.uvsq.coo.ex3_2;

/**
 * @author amine.mezrani (uvsq21506775)
 *
 */
import java.util.ArrayList;

public class Somme {
	ArrayList<Employe> e;
	int nbr_emp;

	int SommeSalaire() {
		int s = 0;
		int j;
		for (j = 0; j < nbr_emp; j++)
			s += e.get(j).Salaire();
		return s;
	}
}
