package fr.uvsq.coo.ex3_2;

/**
 * @author amine.mezrani (uvsq21506775)
 *
 */

public class Employe {
	int annee_prec;
	private final String nom = "";
	private final String adresse = "";

	public String getNom() {
		return nom;
	}

	public String getAdresse() {
		return adresse;
	}

	int Salaire() {
		return 1500 + 20 * this.annee_prec;
	}

}
